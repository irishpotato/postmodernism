import urllib.request
from random import randint
from bs4 import BeautifulSoup


def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()


def get_humoresque():
    # парсим рандомную юмореску с рандомного сайта
    choice = randint(0, 1)
    humoresque = ''
    if choice == 0:
        html = get_html("https://humornet.ru/anekdot/page/" + str(randint(0, 1117)) + "/");
        soup = BeautifulSoup(html)
        humoresques = soup.find('div', id='dle-content').findAll('div', class_='text')
        humoresque = humoresques[randint(0, humoresques.__len__() - 1)].text
    elif choice == 1:
        html = get_html("http://www.anekdot.ws/page/" + str(randint(0, 4054)) + "/");
        soup = BeautifulSoup(html)
        humoresques = soup.find('main', id='main').findAll('div', class_='entry-content')
        humoresque = humoresques[randint(0, humoresques.__len__() - 1)].text
        # убираем мусор
        humoresque = humoresque.replace('(adsbygoogle = window.adsbygoogle || []).push({});', '')

    return humoresque


def get_double_humoresque():
    double_humoresque = ""

    # генерируем первую юмореску    (сетап)
    a = get_humoresque()
    words = a.split(" ")
    # клеим первую юмореску с начала до середины
    for word in range(0, int(words.__len__() / 2)):
        double_humoresque += words[word] + ' '

    # генерируем вторую юмореску
    b = get_humoresque()
    words = b.split(" ")

    # клеим вторую юмореску с середины до конца (панчлайн)
    for word in range(int(words.__len__() / 2), words.__len__()):
        double_humoresque += words[word] + ' '

    return double_humoresque


def main():
    print(get_double_humoresque())


if __name__ == '__main__':
    main()